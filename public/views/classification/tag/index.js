/* global app:true */

(function() {
  'use strict';

  app = app || {};

  app.Record = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      _id: undefined,
      title: '',
      description: ''
    },
    url: function() {
      return '/classification/tag/'+ (this.isNew() ? '' : this.id +'/');
    }
  });

  app.FormView = Backbone.View.extend({
    el: '#form',
    template: _.template( $('#tmpl-form').html() ),
    events: {
      'submit form': 'preventSubmit',
      'keypress input[type="text"]': 'addNewOnEnter',
      'click .btn-add': 'addNew'
    },
    initialize: function() {
      this.model = new app.Record();
      this.listenTo(this.model, 'change', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
    },
    preventSubmit: function(event) {
      event.preventDefault();
    },
    addNewOnEnter: function(event) {
      if (event.keyCode !== 13) { return; }
      event.preventDefault();
      this.addNew();
    },
    addNew: function() {

      if (this.$el.find('[name="title"]').val() === '') {
        alert('Please enter a title.');
      }
      else {
        this.model.save({
          title: this.$el.find('[name="title"]').val(),
          description: this.$el.find('[name="description"]').val()
        },{
          success: function(model, response) {
            if (response.success) {
              console.log('addNew');
              app.formView.model.set({ title: '', description: '' });
              Backbone.history.stop();
              Backbone.history.start();
            }
            else {
              alert(response.errors.join('\n'));
            }
          }
        });
      }
    }
  });

  app.RecordCollection = Backbone.Collection.extend({
    model: app.Record,
    url: '/classification/tag/',
    parse: function(results) {
      return results.data;
    }
  });

  app.ResultsView = Backbone.View.extend({
    el: '#results-table',
    template: _.template( $('#tmpl-results-table').html() ),
    initialize: function() {
      this.collection = new app.RecordCollection( app.mainView.results.data );
      this.listenTo(this.collection, 'reset', this.render);
      this.render();
    },
    render: function() {
      this.$el.html( this.template() );

      var frag = document.createDocumentFragment();
      this.collection.each(function(record) {
        var view = new app.ResultsRowView({ model: record });
        frag.appendChild(view.render().el);
      }, this);
      $('#results-rows').append(frag);

      if (this.collection.length === 0) {
        $('#results-rows').append( $('#tmpl-results-empty-row').html() );
      }

      var table = $('#datatable').DataTable({
        fixedHeader: true
      });

      table.init();
    }
  });

  app.ResultsRowView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template( $('#tmpl-results-row').html() ),
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      return this;
    }
  });

  app.MainView = Backbone.View.extend({
    el: '.right_col .row',
    initialize: function() {
      app.mainView = this;
      this.results = JSON.parse( unescape($('#data-results').html()));
      app.resultsView = new app.ResultsView();

      app.formView = new app.FormView();


    }
  });

  app.Router = Backbone.Router.extend({
    routes: {
      '': 'default',
    },
    initialize: function() {
      app.mainView = new app.MainView();
    },
    default: function() {
      console.log('Router default');
      if (!app.firstLoad) {
        console.log('not first');
        app.resultsView.collection.fetch({ reset: true });
      }

      app.firstLoad = false;
    }
  });

  $(document).ready(function() {
    console.log('firstLoad');
    app.firstLoad = true;
    app.router = new app.Router();
    Backbone.history.start();
  });
}());
