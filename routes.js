'use strict';

exports = module.exports = function(app, passport) {

    var vfPath = "./src/views"; /* view folder path */

    // login/out
    app.get('/auth/login/', require(vfPath + '/auth/login/index').init);
    app.post('/auth/login/', require(vfPath + '/auth/login/index').login);
    app.get('/auth/logout/', require(vfPath + '/auth/logout/index').init);

    app.all('*', ensureAuthenticated);
    // homepage
    app.get('/', require(vfPath + '/index').init);

    app.get('/classification/tag', require(vfPath + '/classification/tag/index').find);
    app.post('/classification/tag', require(vfPath + '/classification/tag/index').add);

    app.get('/settings/organisation/', require(vfPath + '/settings/organisation/index').find);
    app.post('/settings/organisation/', require(vfPath + '/settings/organisation/index').add);
    
    app.all('*', require(vfPath + '/http/index').http404);
};

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.set('X-Auth-Required', 'true');
    req.session.returnUrl = req.originalUrl;
    res.redirect('/auth/login/');
}

function ensureAccount(req, res, next) {
    return next();
}