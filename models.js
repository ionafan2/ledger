"use strict";

/**
 * sort oder is matters
 */
exports = module.exports = function(app, mongoose) {
    require('./src/schema/Note')(app, mongoose);
    require('./src/schema/Status')(app, mongoose);
    require('./src/schema/StatusLog')(app, mongoose);
    require('./src/schema/Category')(app, mongoose);
    require('./src/schema/Classification/Tag')(app, mongoose);
    require('./src/schema/Settings/Organisation')(app, mongoose);
    require('./src/schema/User')(app, mongoose);
    require('./src/schema/Admin')(app, mongoose);
    require('./src/schema/AdminGroup')(app, mongoose);
    require('./src/schema/Account')(app, mongoose);
};
