'use strict';

exports = module.exports = function(app, mongoose) {
  var socialDataSchema = new mongoose.Schema({
    user: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' }
    },
    type: String,
    data: {}
  }, { collection: 'SocialData'.toLowerCase() });

  socialDataSchema.index({ 'user.id': 1 });
  socialDataSchema.index({ type: 1 });
  socialDataSchema.index({ 'user.id': 1, type: 1 }, { unique: true });
  socialDataSchema.set('autoIndex', ('development' === app.get('env')));

  app.db.model('SocialData', socialDataSchema);
}
