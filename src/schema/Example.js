"use strict";

exports = module.exports = function(app, mongoose) {

    var exampleSchema = new mongoose.Schema({
        title: { type: String, unique: true },
        description: { type: String , default: "Not set"}
    });

    app.db.model('Example', exampleSchema);
};
