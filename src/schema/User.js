'use strict';

exports = module.exports = function(app, mongoose) {
  
  var userSchema = new mongoose.Schema({
    password: String,
    email: { type: String, unique: true },
    roles: {
      admin: { type: mongoose.Schema.Types.ObjectId, ref: 'Admin' },
      account: { type: mongoose.Schema.Types.ObjectId, ref: 'Account' }
    },
    isActive: String,
    timeCreated: { type: Date, default: Date.now },
    resetPasswordToken: String,
    resetPasswordExpires: Date
  });

  userSchema.methods.canPlayRoleOf = function(role) {
      return role === "admin" && this.roles.admin;
  };

  userSchema.methods.defaultReturnUrl = function() {
      return '/';
  };

  userSchema.statics.encryptPassword = function(password, done) {
    var bcrypt = require('bcrypt');
    bcrypt.genSalt(10, function(err, salt) {
      if (err) {
        return done(err);
      }
      bcrypt.hash(password, salt, function(err, hash) {
        done(err, hash);
      });
    });
  };

  userSchema.statics.validatePassword = function(password, hash, done) {
    var bcrypt = require('bcrypt');
    bcrypt.compare(password, hash, function(err, res) {
      done(err, res);
    });
  };

  userSchema.index({ email: 1 }, { unique: true });
  userSchema.index({ timeCreated: 1 });
  userSchema.set('autoIndex', (app.get('env') === 'development'));

  app.db.model('User', userSchema);
};
