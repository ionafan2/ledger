'use strict';

exports = module.exports = function(app, mongoose) {

    var organisationSchema = new mongoose.Schema({
        inn: { type: String, unique: true},
        type: { type: String, default: '' },
        name: { type: String, unique: true},
        shortName: { type: String, unique: true},
        fullName: { type: String, unique: true },
        executives: {
            ceo: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
            accountant: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
            auditor: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
        },
        employees: [mongoose.Schema.Types.ObjectId]
        //addresses: []
    }, { collection: 'settings-organisations' });

    organisationSchema.plugin(require('../plugins/pagedFind'));

    organisationSchema.index({ inn: 1 }, { unique: true });
    organisationSchema.index({ name: 1 }, { unique: true });
    organisationSchema.index({ shortName: 1 }, { unique: true });
    organisationSchema.index({ fullName: 1 }, { unique: true });
    organisationSchema.set('autoIndex', (app.get('env') === 'development'));

    app.db.model('SettingsOrganisation', organisationSchema);
};
