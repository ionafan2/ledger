'use strict';

exports = module.exports = function(app, mongoose) {

    var tagSchema = new mongoose.Schema({
        title: { type: String, unique: true},
        description: { type: String, default: '' }
    }, { collection: 'classification-tags' });

    tagSchema.plugin(require('../plugins/pagedFind'));

    tagSchema.index({ title: 1 }, { unique: true });
    tagSchema.set('autoIndex', (app.get('env') === 'development'));

    app.db.model('ClassificationTag', tagSchema);
};
