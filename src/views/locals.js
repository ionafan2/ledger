"use strict";

exports = module.exports = function(app) {

    app.locals.projectName = app.config.projectName;
    app.locals.copyrightYear = new Date().getFullYear();
    app.locals.cacheBreaker = 'br34k-01';

};