"use strict";

exports.find = function(req, res, next){

    req.query.inn = req.query.inn ? req.query.inn : '';
    req.query.type = req.query.type ? req.query.type : '';
    req.query.name = req.query.name ? req.query.name : '';

    req.query.limit = req.query.limit ? parseInt(req.query.limit, null) : 20;
    req.query.page = req.query.page ? parseInt(req.query.page, null) : 1;
    req.query.sort = req.query.sort ? req.query.sort : '_id';

    var filters = {};
    if (req.query.inn) {
        filters.inn = new RegExp('^.*?'+ req.query.inn +'.*$', 'i');
    }
    if (req.query.type) {
        filters.type = new RegExp('^.*?'+ req.query.type +'.*$', 'i');
    }    
    if (req.query.name) {
        filters.name = new RegExp('^.*?'+ req.query.name +'.*$', 'i');
    }

    req.app.db.models.SettingsOrganisation.pagedFind({
        filters: filters,
        keys: 'inn type name',
        limit: req.query.limit,
        page: req.query.page,
        sort: req.query.sort
    }, function(err, results) {
        if (err) {
            return next(err);
        }

        if (req.xhr) {
            res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
            results.filters = req.query;
            res.send(results);
        }
        else {
            results.filters = req.query;
            res.render('settings/organisation/index', { data: { results: escape(JSON.stringify(results)) } });
        }
    });
};

exports.add = function(req, res) {
    var workflow = req.app.utility.workflow(req, res);

    workflow.on('validate', function() {
        if (!req.body.inn) {
            workflow.outcome.errfor.inn = 'required';
        }

        if (workflow.hasErrors()) { return workflow.emit('response'); }

        workflow.emit('add');
    });

    workflow.on('add', function() {

        var fieldsToSet = {
            inn: req.body.inn,
            name: req.body.name,
            type: req.body.type,
            shortName: req.body.name,
            fullName: req.body.name
        };

        req.app.db.models.SettingsOrganisation.create(fieldsToSet, function (err, organisation) {
            workflow.outcome.organisation = organisation;
            organisation.executives.ceo = workflow.user;
            organisation.save(function(err, organisation) {
                if (err) {
                    return workflow.emit('exception', err);
                }

                workflow.emit('response');
            });

        });
    });

    workflow.emit('validate');
};