"use strict";

exports.find = function(req, res, next){
    req.query.title = req.query.title ? req.query.title : '';
    req.query.limit = req.query.limit ? parseInt(req.query.limit, null) : 20;
    req.query.page = req.query.page ? parseInt(req.query.page, null) : 1;
    req.query.sort = req.query.sort ? req.query.sort : '_id';

    var filters = {};
    if (req.query.title) {
        filters.title = new RegExp('^.*?'+ req.query.title +'.*$', 'i');
    }

    req.app.db.models.ClassificationTag.pagedFind({
        filters: filters,
        keys: 'title description',
        limit: req.query.limit,
        page: req.query.page,
        sort: req.query.sort
    }, function(err, results) {
        if (err) {
            return next(err);
        }

        if (req.xhr) {
            res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
            results.filters = req.query;
            res.send(results);
        }
        else {
            results.filters = req.query;
            res.render('classification/tag/index', { data: { results: escape(JSON.stringify(results)) } });
        }
    });
};

exports.add = function(req, res) {
    var workflow = req.app.utility.workflow(req, res);

    workflow.on('validate', function() {
        if (!req.body.title) {
            workflow.outcome.errfor.title = 'required';
        }

        if (workflow.hasErrors()) { return workflow.emit('response'); }

        workflow.emit('add');
    });

    workflow.on('add', function() {

        var fieldsToSet = {
            title: req.body.title,
            description: req.body.description
        };

        req.app.db.models.ClassificationTag.create(fieldsToSet, function (err, tag) {
            workflow.outcome.tag = tag;
            workflow.emit('response');
        });


    });

    workflow.emit('validate');
};