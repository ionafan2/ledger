"use strict";

module.exports = function (grunt) {

    grunt.initConfig({
        jshint: {
            client: {
                options: {
                    jshintrc: 'public/.jshintrc',
                    ignores: [
                        'public/layouts/**/*.min.js',
                        'public/views/**/*.min.js'
                    ]
                },
                src: [
                    'public/layouts/**/*.js',
                    'public/views/**/*.js'
                ]
            },
            server: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: [
                    'src/**/*.js', 'Gruntfile.js', 'app.js', 'routes.js', 'bin/www'
                ]
            }
        },
        watch: {
            clientJS: {
                files: [
                    'public/layouts/**/*.js', '!public/layouts/**/*.min.js',
                    'public/views/**/*.js', '!public/views/**/*.min.js'
                ],
                tasks: ['newer:uglify']
            },
            serverJS: {
                files: ['views/**/*.js'],
                tasks: ['newer:jshint:server']
            },
            clientLess: {
                files: [
                    'public/layouts/**/*.less',
                    'public/views/**/*.less',
                    'public/less/**/*.less'
                ],
                tasks: ['newer:less']
            },
            layoutLess: {
                files: [
                    'public/layouts/**/*.less',
                    'public/less/**/*.less'
                ],
                tasks: ['less:layouts']
            },
            tasks: ['jshint']
        },
        concurrent: {
            dev: {
                tasks: ['nodemon', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        nodemon: {
            dev: {
                script: 'app.js'
            }
        },
        uglify: {
            options: {
                sourceMap: true,
                sourceMapName: function (filePath) {
                    return filePath + '.map';
                },
                mangle: {
                    except: ['jQuery', 'Backbone']
                }
            },
            layouts: {
                files: {
                    'public/layouts/core.min.js': [
                        'public/vendors/jquery/dist/jquery.js',
                        'public/vendors/jquery.cookie/jquery.cookie.js',
                        'public/vendors/underscore/underscore.js',
                        'public/vendors/backbone/backbone.js',
                        'public/vendors/bootstrap/dist/js/bootstrap.js',
                        'public/vendors/fastclick/lib/fastclick.js',
                        'public/vendors/nprogress/nprogress.js',
                        'public/vendors/moment/min/moment.min.js',
                        'public/layouts/core.js'
                    ],
                    'public/layouts/auth.min.js': [
                        'public/layouts/auth.js'
                    ]
                }
            },
            views: {
                files: [{
                    expand: true,
                    cwd: 'public/views/',
                    src: ['**/*.js', '!**/*.min.js'],
                    dest: 'public/views/',
                    ext: '.min.js'
                }]
            }
        },
        less: {
            options: {
                compress: true
            },
            layouts: {
                files: {
                    'public/layouts/core.min.css': [
                        'public/less/bootstrap-build.less',
                        'public/less/font-awesome-build.less',
                        'public/layouts/core.less'
                    ]
                }
            },
            views: {
                files: [{
                    expand: true,
                    cwd: 'public/views/',
                    src: ['**/*.less'],
                    dest: 'public/views/',
                    ext: '.min.css'
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'public/layouts/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/layouts/',
                    ext: '.min.css'
                }]
            }
        },
        clean: {
            js: {
                src: [
                    'public/layouts/**/*.min.js',
                    'public/layouts/**/*.min.js.map',
                    'public/views/**/*.min.js',
                    'public/views/**/*.min.js.map'
                ]
            },
            css: {
                src: [
                    'public/layouts/**/*.min.css',
                    'public/views/**/*.min.css'
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('default', ['newer:uglify', 'newer:less', 'newer:cssmin', 'concurrent']);
    grunt.registerTask('build', ['clean', 'uglify', 'less', 'cssmin']);
    grunt.registerTask('lint', ['jshint']);
    grunt.registerTask('restart', ['build', 'concurrent']);
    grunt.registerTask('sass', ['sass']);
};