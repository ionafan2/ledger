'use strict';

var config = require('./config');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jsend = require('jsend');
var session = require('express-session');
var flash = require('connect-flash');
var mongoStore = require('connect-mongo')(session);
var passport = require('passport');
var helmet = require('helmet');
var csrf = require('csurf');
var debug = require('debug')('ledger:server');
var http = require('http');
var i18n = require("i18n");

var app = express();

//keep reference to config
app.config = config;

//settings
app.disable('x-powered-by');
app.set('port', config.port);
app.set('views', path.join(__dirname, 'src/views'));
app.set('view engine', 'pug');

i18n.configure({
    locales: ['en', 'ru'],
    cookie: 'lang',
    directory: __dirname + '/src/locales'
});

//middleware
app.use(require('morgan')('dev'));
app.use(require('method-override')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser(config.cryptoKey));
app.use(i18n.init);

var sessionMongoStore = new mongoStore({ url: config.mongodb.uri() });

app.use(session({
    key: config.session.key,
    secret: config.cryptoKey,
    store: sessionMongoStore,
    resave: true,
    saveUninitialized: true
}));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(csrf({ cookie: { signed: true } }));
helmet(app);

//response locals
app.use(function(req, res, next) {
    res.cookie('_csrfToken', req.csrfToken());
    res.locals.user = {};
    res.locals.user.defaultReturnUrl = req.user && req.user.defaultReturnUrl();
    res.locals.user.email = req.user && req.user.email;
    next();
});

//setup mongoose
app.db = mongoose.createConnection(config.mongodb.uri());
app.db.on('error', console.error.bind(console, 'mongoose connection error: '));
app.db.once('open', function () {
  //and... we have a data store
});

//setup utilities
app.utility = {};
app.utility.sendmail = require('./src/util/sendmail');
app.utility.workflow = require('./src/util/workflow');

//global locals
require('./src/views/locals')(app);

//config data models
require('./models')(app, mongoose);

//setup passport
require('./passport')(app, passport);

app.use(jsend.middleware);

//setup routes
require('./routes')(app, passport);

//custom (friendly) error handler
app.use(require('./src/views/http/index').http500);

/**
 * Create HTTP server.
 */
var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(config.port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof config.port === 'string' ?
    'Pipe ' + config.port : 'Port ' + config.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
    'pipe ' + addr : 'port ' + addr.port;

    debug('Listening on ' + bind);
}
