'use strict';

exports.port = normalizePort(process.env.PORT || 3000);

exports.mongodb = {
    name: "ledger",
    host: "localhost",
    port: "27017",
    uri: function () {
        return 'mongodb://'+ this.host +':'+ this.port +'/'+ this.name;
    }
};

exports.loginAttempts = {
    forIp: 50,
    forIpAndUser: 7,
    logExpiration: '20m'
};

exports.session = {
    key: 'ledger'
};

exports.projectName = 'Ledger';
exports.cryptoKey = 'cryptoKey';


exports.requireAccountVerification = false;
exports.smtp = {
    from: {
        name: process.env.SMTP_FROM_NAME || exports.projectName +' Website',
        address: process.env.SMTP_FROM_ADDRESS || 'your@email.addy'
    },
    credentials: {
        user: process.env.SMTP_USERNAME || 'your@email.addy',
        password: process.env.SMTP_PASSWORD || 'bl4rg!',
        host: process.env.SMTP_HOST || 'smtp.gmail.com',
        ssl: true
    }
};


/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}